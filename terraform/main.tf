terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  cloud_id = "${var.yc_cloud}"
  folder_id = "${var.yc_folder}"
  token = "${var.yc_token}"
  zone = "${var.yc_zone1}"
}

# Поиск последней версии Ubuntu

data "yandex_compute_image" "my-ubuntu-2204-1" {
  family = "ubuntu-2204-lts"
}

# Сеть

resource "yandex_vpc_network" "yc-personal" {
  name  = "personal"
  folder_id = "${var.yc_folder}"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name = "SubnetA-Practice-project"
  zone = "${var.yc_zone1}"
  network_id = yandex_vpc_network.yc-personal.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_subnet" "subnet-2" {
  name = "SubnetB-Practice-project"
  zone = "${var.yc_zone2}"
  network_id = yandex_vpc_network.yc-personal.id
  v4_cidr_blocks = ["192.168.11.0/24"]
}


# Создание правила разрешающего трафик к нашим серверам

resource "yandex_vpc_security_group" "web" {
  name = "rule-22-80-ports"
  network_id = yandex_vpc_network.yc-personal.id
  ingress {
    description    = "Allow HTTP protocol from local subnets"
    protocol       = "TCP"
    port           = 80
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description    = "Allow SSH protocol from local subnets"
    protocol       = "TCP"
    port           = 22
    v4_cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description    = "Permit ANY"
    protocol       = "ANY"
    v4_cidr_blocks = ["0.0.0.0/0"]
   }
}


# Создание первого инстанса

resource "yandex_compute_instance" "nginx-1" {
  name = "nginx-server-1"
  platform_id = "standard-v1"
  zone = "${var.yc_zone1}"
  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd89n8278rhueakslujo"
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat = true
  }
  
  metadata = {
    user-data = "${file("meta.yml")}"
  }
}

# Создание второго инстанса

resource "yandex_compute_instance" "nginx-2" {
  name = "nginx-server-2"
  platform_id = "standard-v1"
  zone = "${var.yc_zone2}"
  resources {
    cores = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "fd89n8278rhueakslujo"
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-2.id
    nat = true
  }

  metadata = {
    user-data = "${file("meta.yml")}"
  }
}

# Создание третьего инстанса

resource "yandex_compute_instance" "my-webs" {
  name = "my-webserver"
  platform_id = "standard-v1"
  zone = "${var.yc_zone1}"
  resources {
    cores = 2
    memory = 2
  }
  
  boot_disk {
    initialize_params {
      size = 10
      image_id = "fd89n8278rhueakslujo"
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat = true
  }

  metadata = {
    user-data = "${file("meta.yml")}"
  }
}

# Создание целевой группы ВМ

resource "yandex_lb_target_group" "lb-group" {
  name = "nginx-lb-group"
  region_id = "ru-central1"

  target {
    subnet_id = "${yandex_vpc_subnet.subnet-1.id}"
    address = "${yandex_compute_instance.nginx-1.network_interface.0.ip_address}"
  }

  target {
    subnet_id = "${yandex_vpc_subnet.subnet-2.id}"
    address = "${yandex_compute_instance.nginx-2.network_interface.0.ip_address}"
  }
}

# Создание балансировщика нагрузки и обработчика

resource "yandex_lb_network_load_balancer" "yandex-nlb" {
  name = "load-balancer-nginx"
  listener {
    name = "listener-nginx"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }
  attached_target_group {
    target_group_id = yandex_lb_target_group.lb-group.id
    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

# Выведем IP адрес сервера

#output "internal_ip_address_nginx_1" {

output "external_ip_address_nginx_1" {
  value = yandex_compute_instance.nginx-1.network_interface.0.nat_ip_address
}

output "external_ip_address_nginx_2" {
  value = yandex_compute_instance.nginx-2.network_interface.0.nat_ip_address
}

output "external_ip_address_my_webs" {
  value = yandex_compute_instance.my-webs.network_interface.0.nat_ip_address
}
