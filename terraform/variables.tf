variable  yc_zone1 {
  description = "Zone A yandex cloud"
  default = "ru-central1-a"
}

variable yc_zone2 {
  description = "Zone B yandex cloud"
  default = "ru-central1-b"
}

variable yc_token {
  description = "Yandex cloud token"
  default = ""
}

variable yc_cloud {
  description = "ID Yandex Cloud"
  default = ""
}

variable yc_folder {
  description = "ID Yandex Folder"
  default = ""
}

